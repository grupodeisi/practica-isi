import static org.junit.Assert.*;
import org.junit.*;
import static org.junit.Assert.assertTrue;
import java.util.*;
import java.lang.IllegalArgumentException;

public class NumRomanosTests
{
   public String entrada;
   NumRomanos numero = new NumRomanos();

   @Test public void numeroValida() 
   {
      String entrada = "CXIII";
      int valor = numero.romanADecimal(entrada);
      assertEquals( 113, valor);
   }

	
   @Test public void letraNoValida() 
   {
      String entrada = "FLX";
      try {
     	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("Carácter no valida");
   }
   
   @Test public void cuandoVPuedeIrALaIzq() 
   {
      String entrada = "VL";
      try {
    	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("El carácter V solo puede ir a la izquierda de I");
   }
   
   @Test public void cuandoLPuedeIrALaIzq() 
   {
      String entrada = "LC";
      try {
     	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("El carácter L solo puede ir a la izquierda de X, V y I");
   }
   
   @Test public void cuandoXPuedeIrALaIzq() 
   {
      String entrada = "XD";
      try {
    	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("El carácter X no puede ir a la izquierda de D y M");
   }
   
   @Test public void repetirHastaTresVeces() 
   {
      String entrada = "XXXXI";
      try {
   	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("Los caracteres M, C, X y I no se pueden repetir más de tres veces");
   }
   
   @Test public void letrasNoSeRepiten() 
   {
      String entrada = "DDI";
      try {
     	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("Los caracteres D, L y V solo pueden ir una vez");
   }
   
   @Test public void cuandoIPuedeIrALaIzq() 
   {
      String entrada = "IL";
      try {
     	  numero.romanADecimal(entrada);
     }catch (IllegalArgumentException e) {
      	  return;
     }
     fail ("El carácter I solo puede ir a la izquierda de V y X");
   }

   @Test public void cuandoDPuedeIrALaIzq() 
   {
      String entrada = "DM";
      try {
     	  numero.romanADecimal(entrada);
      }catch (IllegalArgumentException e) {
       	  return;
      }
      fail ("El carácter D no puede ir a la izquierda de M");
   }
}
