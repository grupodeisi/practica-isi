import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class EmbotelladoraTest
{
   Embotelladora emb = new Embotelladora();
		
   @Test public void test1() {
      	try{
      		int botellas = emb.calculaBotellasPequenas(10, -3, -9);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   	fail ("NoSolution expected");
   }
   /*
   @Test public void test2() {
      	try{
      		int botellas = emb.calculaBotellasPequenas(1, 1, 5.5);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   	fail ("NoSolution expected");
   }*/
   @Test public void test3() {
   	try{
      		int botellas = emb.calculaBotellasPequenas(5,5,0);
		assertEquals(botellas, 0);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   }
   @Test public void test4() {
	try{
      		int botellas = emb.calculaBotellasPequenas(0, 0, 5);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   	fail ("NoSolution expected");
   }
   @Test public void test5() {
	try{
      		int botellas = emb.calculaBotellasPequenas(3, 0, 7);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   	fail ("NoSolution expected");
   }
   @Test public void test6() {
   	try{
      		int botellas = emb.calculaBotellasPequenas(10, 0, 7);
		assertEquals(botellas, 7);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   }
   @Test public void test7() {
   	try{
      		int botellas = emb.calculaBotellasPequenas(5, 5, 7);
		assertEquals(botellas, 2);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   }
   @Test public void test8() {
	try{
      		int botellas = emb.calculaBotellasPequenas(1, 3, 17);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   	fail ("NoSolution expected");
   }
   @Test public void test9() {
	try{
      		int botellas = emb.calculaBotellasPequenas(10, 3, 25);
      		assertEquals(botellas, 10);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   }
   
   @Test public void test10() {
   	try{
      		int botellas = emb.calculaBotellasPequenas(25, 3, 30);
		assertEquals(botellas, 15);
      	}  catch (Embotelladora.NoSolution e) {
        	return;
   	}
   }
}


