import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;
import java.lang.IllegalArgumentException;

public class BisiestosTest
{
   Bisiestos ano = new Bisiestos();

   @Test public void AnoNegativo() 
   {
      int a = -2;
      try{
      	ano.esBisiesto(a);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }
  
   @Test public void YearZero() 
   {
      int a = 0;
      try{
      	ano.esBisiesto(a);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }
   
//si consideramos 0 como no valido, poner como el caso anterior con IllegalArgumentException
 
 //Probar divisible entre 4, 100, 400, los casos frontera descritos en la parametrización.
 
   @Test public void ValidNotLeapYear() 
   {
      int a = 3;//numero valido, no bisiesto, un año positivo no bisiesto
      assertFalse(ano.esBisiesto(a));
   }

   @Test public void LeapYear1() 
   {
      int a = 12;//multiplo de 4 pero no de 100
      assertTrue(ano.esBisiesto(a));
   }

   @Test public void LeapYear() 
   {
      int a = 2016;
      assertTrue(ano.esBisiesto(a));
   }

   @Test public void NotLeapYear2() 
   {
      int a = 200;//multiplo de 4 y 100 pero no de 400
      assertFalse(ano.esBisiesto(a));
   }

}
