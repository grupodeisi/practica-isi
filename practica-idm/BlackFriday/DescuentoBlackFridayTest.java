import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;
import java.util.Calendar;
import java.lang.IllegalArgumentException;

public class DescuentoBlackFridayTest
{
   DescuentoBlackFriday descuento = new DescuentoBlackFriday();
   Calendar fecha = Calendar.getInstance();
         
   @Test public void precioOriginalNegativo() 
   {
      double precioOriginal = -2.99;
      double porcentajeDescuento = 1.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }
   
   @Test public void porcentajeDescuentoNegativo() 
   {
      double precioOriginal = 2.99;
      double porcentajeDescuento = -1.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }

   @Test public void Negativos() 
   {
      double precioOriginal = -2.99;
      double porcentajeDescuento = -1.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }

   @Test public void precioOriginalCero() 
   {
      double precioOriginal = 0.00;
      double porcentajeDescuento = 1.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }
 
   @Test public void porcentajeDescuentoCero() 
   {
      double precioOriginal = 2.99;
      double porcentajeDescuento = 0.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }
  
   @Test public void SinPrecioNiDescuento() 
   {
      double precioOriginal = 0.00;
      double porcentajeDescuento = 0.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }
   
   @Test public void porcentajeDescuento100() 
   {
      double precioOriginal = 2.99;
      double porcentajeDescuento = 100.00;
      
      try{
      	descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      }catch (IllegalArgumentException e){
      	return;
      }
      fail ("IllegalArgumentException"); 
   }   
  
  
   @Test public void OriginalPrice() 
   {
      //Calendar fecha = Calendar(2021,10,29);
      double precioOriginal = 56.56;
      double porcentajeDescuento = 50.00;
      try{
      	double preciofinal = descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      	assertEquals( 56.56, preciofinal, 0.1);
      }catch (IllegalArgumentException e){
      	fail ("IllegalArgumentException"); 
      	return;
      }
   }  
   
   @Test public void HalfPrice() 
   {
      fecha.set(2000, Calendar.NOVEMBER, 29);
      
      double precioOriginal = 56.56;
      double porcentajeDescuento = 50.00;
      try{
      	double preciofinal = descuento.precioFinal(precioOriginal, porcentajeDescuento, fecha);
      	assertEquals( 28.28, preciofinal, 0.1);
      }catch (IllegalArgumentException e){
      	fail ("IllegalArgumentException"); 
      	return;
      }
   }  
}
