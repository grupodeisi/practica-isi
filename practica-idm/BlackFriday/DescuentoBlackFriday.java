import java.io.*;
import java.lang.IllegalArgumentException;
import java.util.Calendar;

public class DescuentoBlackFriday
{
  /**
    * Calcula el precio con descuento durante el BlackFriday
    * 
    *  @param precioOriginal es el precio de un producto marcado en la etiqueta
    *  @param porcentajeDescuento es el descuento a aplicar
    *  @return el precio final
    *  @throws IllegalArgumentException si precioOriginal es negativo o porcentajeDescuento es negativo
    */
    	public boolean blackfriday(Calendar fecha)
    	{
    		int mes = fecha.get(Calendar.MONTH);
    		int dia = fecha.get(Calendar.DAY_OF_MONTH);
    		if(mes == 10 && dia == 29){
    			return true;
    		}else{
    			return false;
    		}	
    	}
    
    
	public double precioFinal(double precioOriginal, double porcentajeDescuento, Calendar fecha) throws IllegalArgumentException
	{ 
	if(precioOriginal <= 0 || porcentajeDescuento >= 100 || porcentajeDescuento <= 0){//porcentajeDescuento debe estar entre (0-100)
		throw new IllegalArgumentException("parámetros no válidos"); 
	} 
	//Calendar a partir de aqui 
	//double precioDescuento = precioOriginal*(porcentajeDescuento/100);
	//double precioFinal = precioOriginal - precioDescuento;
	//return precioFinal;
	//}    
	
	if(blackfriday(fecha)){//usar Calendar
		double precioDescuento = precioOriginal*(porcentajeDescuento/100);
		double precioFinal = precioOriginal - precioDescuento;
		return precioFinal;
	}else{
		return precioOriginal;
	}
	}	
}
