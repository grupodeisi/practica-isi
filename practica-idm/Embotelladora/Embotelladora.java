public class Embotelladora {
	// @param pequenas: n ́umero de botellas en almac ́en de 1L
	// grandes : n ́umero de botellas en almac ́en de 5L
	// total : n ́umero total de litros que hay que embotellar
	// @return n ́umero de botellas peque~nas necesarias para embotellar el total de l ́ıquido, teniendo
	// en cuenta que hay que minimizar el n ́umero de botellas peque~nas: primero
	// se rellenan las grandes
	// @throws NoSolution si no es posible embotellar todo el l ́ıquido
	
	public class NoSolution extends Exception {
    		public NoSolution() {
        		;
    		}
	}
	
	public int calculaBotellasPequenas(int pequenas, int grandes, int total) throws NoSolution {
		int litrosR = total;
		int grandesU = 0;
		int pequenasU = 0;
		
		if (pequenas<0 || grandes<0 || total<0 || (pequenas%10)!=0 || (grandes%10)!=0 || (total%10)!=0){
			throw new NoSolution();
		}
		
		if (litrosR > 5){
			grandesU = litrosR/5 - litrosR%5;
			if (grandesU > grandes) grandesU=grandes;
			litrosR = total - grandesU*5;
		}
		if (litrosR > pequenas) throw new NoSolution();
		
		return litrosR;
	}

	   
}
