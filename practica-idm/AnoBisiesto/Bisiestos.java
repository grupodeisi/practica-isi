import java.io.*;
import java.lang.IllegalArgumentException;

public class Bisiestos {
// @param a un entero positivo
// @return true año bisiesto
//	   false no año bisiesto
// @throws IllegalArgumentException si a no es un parámetro válido.
	public boolean esBisiesto(int a) throws IllegalArgumentException {
		if(a <= 0){
			throw new IllegalArgumentException("El año debe ser un número entero positivo");
		}
		if((a % 4 == 0 && a % 100 != 0) || a % 400 == 0){
			return true;
		}else{
			return false;
		}
	}
}
