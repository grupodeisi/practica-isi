import java.io.*;
import java.lang.IllegalArgumentException;
import java.security.InvalidParameterException;
import java.util.Hashtable;

public class NumRomanos 
{
	public static Hashtable<String, Integer> hacerTabla()
	{
		Hashtable<String, Integer> numRom = new Hashtable<String, Integer>();
		numRom.put(" ", 0);
		numRom.put("I", 1);
		numRom.put("V", 5);
		numRom.put("X", 10);
		numRom.put("L", 50);
		numRom.put("C", 100);
		numRom.put("D", 500);
		numRom.put("M", 1000);
		
		return numRom;
	}
	
	public int romanADecimal(String entrada) throws InvalidParameterException{
		int valor = 0;
		int salida = 0;
		int numant = 0;
		char letra;
		boolean correcto;
		letrasPermitenRepeticion(entrada);
		letrasProhibidasRepeticion(entrada);
		for(int i = 0; i < entrada.length(); i++){
			letra = entrada.charAt(i);
			Hashtable<String, Integer> numRom = hacerTabla();
			correcto = numRom.containsKey(Character.toString(letra).toUpperCase());
			if(correcto) {
				valor = numRom.get(Character.toString(letra).toUpperCase());
				if(numant < valor && numant != 0){
					salida = salida - numant;
				}else{
					salida = salida + valor;
				} 
				numant = valor;
			}else{
				throw new InvalidParameterException("No existe");
			}
		}
		if (esCorrecto(entrada)){
			return salida;
		}else{
			throw new InvalidParameterException("La entrada no es correcta");	
		}
	}	
	
	public static boolean esCorrecto(String numero){
		return !numero.contains("IL") && !numero.contains("IC") && !numero.contains("ID")
	    		&& !numero.contains("IM") && !numero.contains("VL") && !numero.contains("VC")
	    		&& !numero.contains("VD") && !numero.contains("VM") && !numero.contains("XD")
	    		&& !numero.contains("XM") && !numero.contains("DM") && !numero.contains("LC") 
	    		&& !numero.contains("LD") && !numero.contains("LM");
	}
	
	
	public static void letrasPermitenRepeticion(String x){
		char [] letrasPermiten = {'I', 'X', 'C', 'M'};
		int contador = 0;
		for (int i = 0; i < letrasPermiten.length; i++){
			char letra = letrasPermiten[i];
			for (int j = 0; j < x.length() - 3; j++) {
				if (x.charAt(j) == letra && x.charAt(j+1) == letra && x.charAt(j+2) == letra && x.charAt(j+3) == letra){
					throw new IllegalArgumentException("Letra repetida más de tres veces");
				}
			}
		}
	}
	
	public static void letrasProhibidasRepeticion(String x){
		char [] letrasProhibidas = {'V', 'L', 'D'};
		int contador = 0;
		for (int i = 0; i < letrasProhibidas.length; i++){
			char letra = letrasProhibidas[i];
			for (int j = 0; j < x.length() - 1; j++) {
				if (x.charAt(j) == letra && x.charAt(j+1) == letra){
					throw new IllegalArgumentException("Las letras V, L y D no se pueden repetir");
				}
			}
		}
	}
}
	
	
	
	
	
	
	
